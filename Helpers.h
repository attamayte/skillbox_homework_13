#pragma once

// Perhaps function definitions should be put in Helpers.cpp
// Was using macros to check func signatures verifying valid type instancing


// a simple functon that sum it's arguments and return squared result
template <typename T>
T SquareSum(const T a, const T b)
{
#if _DEBUG
	std::cout << __FUNCSIG__ << '\n';
#endif
	return (a + b) * (a + b);
}

// basically same as above, but return sum of squared arguments
template <typename T>
T SumSquares(const T a, const T b)
{
#if _DEBUG
	std::cout << __FUNCSIG__ << '\n';
#endif
	return (a * a) + (b * b);
}