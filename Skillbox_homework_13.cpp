#include <iostream>
#include "Helpers.h"

// a little demo for both template functions
int main()
{
    int egerValue{ SquareSum(4, 5) }; 
    float ingPoint{ SquareSum(3.14F, 4.13F) };
    double trouble{ SumSquares(2.22, 1.11) };
    char acter{ SumSquares('a', 'b') }; // would certainly cause <char> to overflow; not an issue here, though 

    std::cout << "SquareSum(4, 5) = " << egerValue << "\nSquareSum(3.14F, 4.13F) = " << ingPoint
        << "\nSumSquares(2.22, 1.11) = " << trouble << "\nSumSquares('a', 'b') = " << acter << '\n';

    return 0;
}

